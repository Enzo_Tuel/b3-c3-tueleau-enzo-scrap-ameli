import requests
from bs4 import BeautifulSoup
import csv

payload = {
    "type": "ps",
    "ps_profession": "34",
    "ps_profession_label": "Médecin généraliste",
    "ps_localisation": "HERAULT (34)",
    "localisation_category": "departements",
}

header = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36",
}
url = "http://annuairesante.ameli.fr/recherche.html"
page = requests.post(url, params=payload, headers=header)
soup = BeautifulSoup(page.text, "html.parser")
divs = soup.find_all("div", class_="item-professionnel")

with open('doctors.csv', 'w', newline='') as csvfile:
    csv_writer = csv.writer(csvfile)
    csv_writer.writerow(['Nom et prenom', 'Adresse', 'Numéro de telephone'])
    for div in divs:
        div_pictos = div.find("div", class_="nom_pictos").text
        div_adress = div.find("div", class_="adresse").text
        div_num = div.find("div", class_="tel").text if div.find("div", class_="tel") else 'Pas de numero'
        csv_writer.writerow([div_pictos, div_adress, div_num])



